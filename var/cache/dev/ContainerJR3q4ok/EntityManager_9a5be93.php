<?php

namespace ContainerJR3q4ok;
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';

class EntityManager_9a5be93 extends \Doctrine\ORM\EntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    /**
     * @var \Doctrine\ORM\EntityManager|null wrapped object, if the proxy is initialized
     */
    private $valueHolder383cf = null;

    /**
     * @var \Closure|null initializer responsible for generating the wrapped object
     */
    private $initializerfbc73 = null;

    /**
     * @var bool[] map of public properties of the parent class
     */
    private static $publicProperties4b4de = [
        
    ];

    public function getConnection()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getConnection', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getConnection();
    }

    public function getMetadataFactory()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getMetadataFactory', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getMetadataFactory();
    }

    public function getExpressionBuilder()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getExpressionBuilder', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getExpressionBuilder();
    }

    public function beginTransaction()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'beginTransaction', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->beginTransaction();
    }

    public function getCache()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getCache', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getCache();
    }

    public function transactional($func)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'transactional', array('func' => $func), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->transactional($func);
    }

    public function wrapInTransaction(callable $func)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'wrapInTransaction', array('func' => $func), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->wrapInTransaction($func);
    }

    public function commit()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'commit', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->commit();
    }

    public function rollback()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'rollback', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->rollback();
    }

    public function getClassMetadata($className)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getClassMetadata', array('className' => $className), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getClassMetadata($className);
    }

    public function createQuery($dql = '')
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'createQuery', array('dql' => $dql), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->createQuery($dql);
    }

    public function createNamedQuery($name)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'createNamedQuery', array('name' => $name), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->createNamedQuery($name);
    }

    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->createNativeQuery($sql, $rsm);
    }

    public function createNamedNativeQuery($name)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->createNamedNativeQuery($name);
    }

    public function createQueryBuilder()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'createQueryBuilder', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->createQueryBuilder();
    }

    public function flush($entity = null)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'flush', array('entity' => $entity), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->flush($entity);
    }

    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->find($className, $id, $lockMode, $lockVersion);
    }

    public function getReference($entityName, $id)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getReference($entityName, $id);
    }

    public function getPartialReference($entityName, $identifier)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getPartialReference($entityName, $identifier);
    }

    public function clear($entityName = null)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'clear', array('entityName' => $entityName), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->clear($entityName);
    }

    public function close()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'close', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->close();
    }

    public function persist($entity)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'persist', array('entity' => $entity), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->persist($entity);
    }

    public function remove($entity)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'remove', array('entity' => $entity), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->remove($entity);
    }

    public function refresh($entity)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'refresh', array('entity' => $entity), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->refresh($entity);
    }

    public function detach($entity)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'detach', array('entity' => $entity), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->detach($entity);
    }

    public function merge($entity)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'merge', array('entity' => $entity), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->merge($entity);
    }

    public function copy($entity, $deep = false)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->copy($entity, $deep);
    }

    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->lock($entity, $lockMode, $lockVersion);
    }

    public function getRepository($entityName)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getRepository', array('entityName' => $entityName), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getRepository($entityName);
    }

    public function contains($entity)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'contains', array('entity' => $entity), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->contains($entity);
    }

    public function getEventManager()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getEventManager', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getEventManager();
    }

    public function getConfiguration()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getConfiguration', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getConfiguration();
    }

    public function isOpen()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'isOpen', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->isOpen();
    }

    public function getUnitOfWork()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getUnitOfWork', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getUnitOfWork();
    }

    public function getHydrator($hydrationMode)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getHydrator($hydrationMode);
    }

    public function newHydrator($hydrationMode)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->newHydrator($hydrationMode);
    }

    public function getProxyFactory()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getProxyFactory', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getProxyFactory();
    }

    public function initializeObject($obj)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'initializeObject', array('obj' => $obj), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->initializeObject($obj);
    }

    public function getFilters()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'getFilters', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->getFilters();
    }

    public function isFiltersStateClean()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'isFiltersStateClean', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->isFiltersStateClean();
    }

    public function hasFilters()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'hasFilters', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return $this->valueHolder383cf->hasFilters();
    }

    /**
     * Constructor for lazy initialization
     *
     * @param \Closure|null $initializer
     */
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;

        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();

        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);

        $instance->initializerfbc73 = $initializer;

        return $instance;
    }

    protected function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, \Doctrine\Common\EventManager $eventManager)
    {
        static $reflection;

        if (! $this->valueHolder383cf) {
            $reflection = $reflection ?? new \ReflectionClass('Doctrine\\ORM\\EntityManager');
            $this->valueHolder383cf = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);

        }

        $this->valueHolder383cf->__construct($conn, $config, $eventManager);
    }

    public function & __get($name)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, '__get', ['name' => $name], $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        if (isset(self::$publicProperties4b4de[$name])) {
            return $this->valueHolder383cf->$name;
        }

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder383cf;

            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder383cf;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __set($name, $value)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, '__set', array('name' => $name, 'value' => $value), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder383cf;

            $targetObject->$name = $value;

            return $targetObject->$name;
        }

        $targetObject = $this->valueHolder383cf;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;

            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();

        return $returnValue;
    }

    public function __isset($name)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, '__isset', array('name' => $name), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder383cf;

            return isset($targetObject->$name);
        }

        $targetObject = $this->valueHolder383cf;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();

        return $returnValue;
    }

    public function __unset($name)
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, '__unset', array('name' => $name), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        $realInstanceReflection = new \ReflectionClass('Doctrine\\ORM\\EntityManager');

        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder383cf;

            unset($targetObject->$name);

            return;
        }

        $targetObject = $this->valueHolder383cf;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);

            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }

    public function __clone()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, '__clone', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        $this->valueHolder383cf = clone $this->valueHolder383cf;
    }

    public function __sleep()
    {
        $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, '__sleep', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;

        return array('valueHolder383cf');
    }

    public function __wakeup()
    {
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }

    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializerfbc73 = $initializer;
    }

    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializerfbc73;
    }

    public function initializeProxy() : bool
    {
        return $this->initializerfbc73 && ($this->initializerfbc73->__invoke($valueHolder383cf, $this, 'initializeProxy', array(), $this->initializerfbc73) || 1) && $this->valueHolder383cf = $valueHolder383cf;
    }

    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder383cf;
    }

    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder383cf;
    }
}

if (!\class_exists('EntityManager_9a5be93', false)) {
    \class_alias(__NAMESPACE__.'\\EntityManager_9a5be93', 'EntityManager_9a5be93', false);
}
