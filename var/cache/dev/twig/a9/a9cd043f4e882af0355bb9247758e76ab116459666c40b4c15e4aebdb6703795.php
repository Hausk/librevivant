<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* photos/index.html.twig */
class __TwigTemplate_8acaae018438dab8b5c5fe1e4ac435a301e89a4b664a95b3131473a25d1c5ce7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photos/index.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "photos/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "photos/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Hello PhotosController!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<div class=\"site-wrap\" id=\"home-section\">

      <div class=\"site-mobile-menu site-navbar-target\">
        <div class=\"site-mobile-menu-header\">
          <div class=\"site-mobile-menu-close mt-3\">
            <span class=\"icon-close2 js-menu-toggle\"></span>
          </div>
        </div>
        <div class=\"site-mobile-menu-body\"></div>
      </div>



      <header class=\"site-navbar site-navbar-target\" role=\"banner\">

        <div class=\"container\">
          <div class=\"row align-items-center position-relative\">

            <div class=\"col-3 \">
              <div class=\"site-logo\">
                <a href=\"index.html\">L&V</a>
              </div>
            </div>

            <div class=\"col-9  text-right\">
              <span class=\"d-inline-block d-lg-none\"><a href=\"#\" class=\"text-white site-menu-toggle js-menu-toggle py-5 text-white\"><span class=\"icon-menu h3 text-white\"></span></a></span>

              

              <nav class=\"site-navigation text-right ml-auto d-none d-lg-block\" role=\"navigation\">
                <ul class=\"site-menu main-menu js-clone-nav ml-auto \">
                  <li class=\"\"><a href=\"/\" class=\"nav-link\">Home</a></li>
                  <li class=\"active\"><a href=\"/photos\" class=\"nav-link\">Toutes mes photos</a></li>
                  <li><a href=\"/contact\" class=\"nav-link\">Contact</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

        </header>
        <div class=\"ftco-blocks-cover-1 w-100 h-100\" style=\"height: auto; width: 100vw !important\">
                <div class=\"site-section-cover overlay home-banner h-100\" data-stellar-background-ratio=\"0.5\">
                        <div class=\"container h-100\">
                                <div class=\"row align-items-center justify-content-center text-center h-100\">
                                        <div class=\"col-md-12\">
                                                <h1 class=\"mb-3\">Libre & Vivant</h1>
                                                <p style=\"font-family: 'Bebas Neue', cursive;\">Bonjour, je m’appelle Victoria, je suis agée de 20 ans, j’ai fait de la photographie une passion qui permet de rendre heureux les personnes ou de laisser une émotion sur certaines photographies que personne n'arriverait à voir. Aujourd’hui je me présente a vous afin de pouvoir rendre vos moments encore plus magique et d’en faire des souvenirs inoubliable.</p>
                                                <blockquote class=\"blockquote-footer text-white\" style=\"font-family: 'Calligraffitti', cursive;\">La photographie est une brève complicité entre la prévoyance et le hasard. </blockquote>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        <main class=\"page-content container\">
                <div class=\"category-sel d-flex justify-content-center button-group filter-button-group my-5\">
                        <button class=\"cat-btn cat-btn-first h-100\" data-filter=\"*\">All</button>
                        ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) || array_key_exists("categories", $context) ? $context["categories"] : (function () { throw new RuntimeError('Variable "categories" does not exist.', 65, $this->source); })()));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 66
            echo "                                <button class=\"cat-btn cat-btn h-100 ";
            if ((twig_get_attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 66) % 2 != 0)) {
                echo "even";
            }
            if (twig_get_attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 66)) {
                echo " cat-btn-last";
            }
            echo "\" data-filter=\".";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 66), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["category"], "name", [], "any", false, false, false, 66), "html", null, true);
            echo "</button>
                        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "                </div>

                <div id=\"pictures-grid\" class=\"row w-100 justify-content-center\" data-masonry='{ \"percentPosition\": true}'>
                ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) || array_key_exists("data", $context) ? $context["data"] : (function () { throw new RuntimeError('Variable "data" does not exist.', 71, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["datum"]) {
            // line 72
            echo "                        <div class=\"col-lg-4 col-6 p-2 testing ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["datum"], "category", [], "any", false, false, false, 72), "html", null, true);
            echo "\">
                                <div class=\"p-1 bg-white overflow-hidden w-100\">
                                        <div class=\"w-100 overflow-hidden\">
                                                <img src=\"";
            // line 75
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter($this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["datum"], "imageFile"), "photolist"), "html", null, true);
            echo "\" class=\"m-auto testoverflow w-100\" style=\"overflow: hidden\"/>
                                        </div>
                                </div>
                        </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['datum'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 80
        echo "                </div>
        </main>

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js\"></script>
<script src=\"https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js\"></script>
<script src=\"https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js\"></script>
<script>
// init Isotope
var \$grid = \$('#pictures-grid').isotope({
  // options
});
// filter items on button click
\$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = \$(this).attr('data-filter');
  \$grid.isotope({ filter: filterValue });
});
</script>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "photos/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 80,  208 => 75,  201 => 72,  197 => 71,  192 => 68,  166 => 66,  149 => 65,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Hello PhotosController!{% endblock %}

{% block body %}
<div class=\"site-wrap\" id=\"home-section\">

      <div class=\"site-mobile-menu site-navbar-target\">
        <div class=\"site-mobile-menu-header\">
          <div class=\"site-mobile-menu-close mt-3\">
            <span class=\"icon-close2 js-menu-toggle\"></span>
          </div>
        </div>
        <div class=\"site-mobile-menu-body\"></div>
      </div>



      <header class=\"site-navbar site-navbar-target\" role=\"banner\">

        <div class=\"container\">
          <div class=\"row align-items-center position-relative\">

            <div class=\"col-3 \">
              <div class=\"site-logo\">
                <a href=\"index.html\">L&V</a>
              </div>
            </div>

            <div class=\"col-9  text-right\">
              <span class=\"d-inline-block d-lg-none\"><a href=\"#\" class=\"text-white site-menu-toggle js-menu-toggle py-5 text-white\"><span class=\"icon-menu h3 text-white\"></span></a></span>

              

              <nav class=\"site-navigation text-right ml-auto d-none d-lg-block\" role=\"navigation\">
                <ul class=\"site-menu main-menu js-clone-nav ml-auto \">
                  <li class=\"\"><a href=\"/\" class=\"nav-link\">Home</a></li>
                  <li class=\"active\"><a href=\"/photos\" class=\"nav-link\">Toutes mes photos</a></li>
                  <li><a href=\"/contact\" class=\"nav-link\">Contact</a></li>
                </ul>
              </nav>
            </div>

            
          </div>
        </div>

        </header>
        <div class=\"ftco-blocks-cover-1 w-100 h-100\" style=\"height: auto; width: 100vw !important\">
                <div class=\"site-section-cover overlay home-banner h-100\" data-stellar-background-ratio=\"0.5\">
                        <div class=\"container h-100\">
                                <div class=\"row align-items-center justify-content-center text-center h-100\">
                                        <div class=\"col-md-12\">
                                                <h1 class=\"mb-3\">Libre & Vivant</h1>
                                                <p style=\"font-family: 'Bebas Neue', cursive;\">Bonjour, je m’appelle Victoria, je suis agée de 20 ans, j’ai fait de la photographie une passion qui permet de rendre heureux les personnes ou de laisser une émotion sur certaines photographies que personne n'arriverait à voir. Aujourd’hui je me présente a vous afin de pouvoir rendre vos moments encore plus magique et d’en faire des souvenirs inoubliable.</p>
                                                <blockquote class=\"blockquote-footer text-white\" style=\"font-family: 'Calligraffitti', cursive;\">La photographie est une brève complicité entre la prévoyance et le hasard. </blockquote>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        <main class=\"page-content container\">
                <div class=\"category-sel d-flex justify-content-center button-group filter-button-group my-5\">
                        <button class=\"cat-btn cat-btn-first h-100\" data-filter=\"*\">All</button>
                        {% for category in categories %}
                                <button class=\"cat-btn cat-btn h-100 {% if loop.index is odd %}even{% endif %}{% if loop.last %} cat-btn-last{% endif %}\" data-filter=\".{{category.name}}\">{{category.name}}</button>
                        {% endfor %}
                </div>

                <div id=\"pictures-grid\" class=\"row w-100 justify-content-center\" data-masonry='{ \"percentPosition\": true}'>
                {% for datum in data %}
                        <div class=\"col-lg-4 col-6 p-2 testing {{datum.category}}\">
                                <div class=\"p-1 bg-white overflow-hidden w-100\">
                                        <div class=\"w-100 overflow-hidden\">
                                                <img src=\"{{vich_uploader_asset(datum, 'imageFile')|imagine_filter('photolist')}}\" class=\"m-auto testoverflow w-100\" style=\"overflow: hidden\"/>
                                        </div>
                                </div>
                        </div>
                {% endfor %}
                </div>
        </main>

<script src=\"https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js\"></script>
<script src=\"https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.js\"></script>
<script src=\"https://unpkg.com/isotope-layout@3/dist/isotope.pkgd.js\"></script>
<script>
// init Isotope
var \$grid = \$('#pictures-grid').isotope({
  // options
});
// filter items on button click
\$('.filter-button-group').on( 'click', 'button', function() {
  var filterValue = \$(this).attr('data-filter');
  \$grid.isotope({ filter: filterValue });
});
</script>
{% endblock %}
", "photos/index.html.twig", "/opt/qivalio/libre/templates/photos/index.html.twig");
    }
}
