<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* home/index.html.twig */
class __TwigTemplate_518c46dea831bd0586c6a8a668661707 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "home/index.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "home/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Libre &amp; vivant | Page d'accueil";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_5a27a8ba21ca79b61932376b2fa922d2 = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->enter($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_6f47bbe9983af81f1e7450e9a3e3768f = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->enter($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "<style>
@import url('https://fonts.googleapis.com/css2?family=Blaka&family=Calligraffitti&display=swap');
</style>

<div class=\"site-wrap\" id=\"home-section\">

        <div class=\"site-mobile-menu site-navbar-target\">
                <div class=\"site-mobile-menu-header\">
                        <div class=\"site-mobile-menu-close mt-3\">
                                <span class=\"icon-close2 js-menu-toggle\"></span>
                        </div>
                </div>
                <div class=\"site-mobile-menu-body\"></div>
        </div>
        <header class=\"site-navbar site-navbar-target\" role=\"banner\">
                <div class=\"container\">
                        <div class=\"row align-items-center position-relative\">
                                <div class=\"col-3 \">
                                        <div class=\"site-logo\">
                                                <a href=\"index.html\">L&V</a>
                                        </div>
                                </div>
                                <div class=\"col-9  text-right\">
                                        <span class=\"d-inline-block d-lg-none\">
                                                <a href=\"#\" class=\"text-white site-menu-toggle js-menu-toggle py-5 text-white\">
                                                        <span class=\"icon-menu h3 text-white\"></span>
                                                </a>
                                        </span>
                                        <nav class=\"site-navigation text-right ml-auto d-none d-lg-block\" role=\"navigation\">
                                                <ul class=\"site-menu main-menu js-clone-nav ml-auto \">
                                                        <li class=\"active\"><a href=\"/\" class=\"nav-link\">Accueil</a></li>
                                                        <li><a href=\"/photos\" class=\"nav-link\">Toutes mes photos</a></li>
                                                        <li><a href=\"/contact\" class=\"nav-link\">Contact</a></li>
                                                </ul>
                                        </nav>
                                </div>
                        </div>
                </div>
        </header>
        <div class=\"ftco-blocks-cover-1 w-100\" style=\"height: auto\">
                <div class=\"site-section-cover overlay home-banner h-100\" data-stellar-background-ratio=\"0.5\">
                        <div class=\"container h-100\">
                                <div class=\"row align-items-center justify-content-center text-center h-100\">
                                        <div class=\"col-md-12\">
                                                <h1 class=\"mb-3\">Libre & Vivant</h1>
                                                <p style=\"font-family: 'Bebas Neue', cursive;\">Bonjour, je m’appelle Victoria, je suis agée de 20 ans, j’ai fait de la photographie une passion qui permet de rendre heureux les personnes ou de laisser une émotion sur certaines photographies que personne n'arriverait à voir. Aujourd’hui je me présente a vous afin de pouvoir rendre vos moments encore plus magique et d’en faire des souvenirs inoubliable.</p>
                                                <blockquote class=\"blockquote-footer text-white\" style=\"font-family: 'Calligraffitti', cursive;\">La photographie est une brève complicité entre la prévoyance et le hasard. </blockquote>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        <div class=\"site-section p-2 bg-black\">
                <div class=\"row\">
                        <div class=\"col-lg-12 ml-auto pl-lg-5 text-center pt-4\">
                                <h3 class=\"scissors text-center text-red\">Dernières prises!</h3>
                                <div class=\"row w-100\">
                                        <div class=\"col-12\">
                                                <div class=\"price-table pb-4\">
                                                        ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["photos"]) || array_key_exists("photos", $context) ? $context["photos"] : (function () { throw new RuntimeError('Variable "photos" does not exist.', 65, $this->source); })()));
        foreach ($context['_seq'] as $context["_key"] => $context["photo"]) {
            // line 66
            echo "                                                                <div class=\"product-card pt-0 p-1\">
                                                                        <div class=\"w-100\">
                                                                                <img class=\"w-100\" src=\"";
            // line 68
            echo twig_escape_filter($this->env, $this->extensions['Liip\ImagineBundle\Templating\FilterExtension']->filter($this->extensions['Vich\UploaderBundle\Twig\Extension\UploaderExtension']->asset($context["photo"], "imageFile"), "homepage"), "html", null, true);
            echo "\"/>
                                                                        </div>
                                                                        <div class=\"d-flex justify-content-end flex-column my-2\">
                                                                                <p class=\"font-weight-bold\">";
            // line 71
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "name", [], "any", false, false, false, 71), "html", null, true);
            echo "</p>
                                                                                <p class=\"text-muted m-0\">Ajouté le: ";
            // line 72
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["photo"], "postDate", [], "any", false, false, false, 72), "d-m-Y"), "html", null, true);
            echo "</p>
                                                                        </div>
                                                                </div>
                                                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['photo'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 76
        echo "                                                </div>
                                        </div>
                                </div>
                                <p class=\"p-5\"><a href=\"/photos\" class=\"btn btn-more\">En voir plus</a></p>
                        </div>
                </div>
        </div>
        <div class=\"site-section bg-black p-0 pt-2\">
                <div class=\"container\">
                        <div class=\"row justify-content-center  mb-5\">
                                <div class=\"col-md-7 text-center\">
                                        <h3 class=\"scissors text-center\">Services &amp; Prix</h3>
                                </div>
                        </div>
                        <div class=\"row\">
                                <div class=\"col-12\">
                                        <div class=\"price-table pb-4\">
                                                <div class=\"price-card\">
                                                        <h2 class=\"price-card__title\">100 prises</h2>
                                                        <div class=\"price-card__ribbon\"><span class=\"price-card__ribbon-text\">25 €</span></div>
                                                        <p>100 Photos</p>
                                                        <p>Individuel & groupe</p>
                                                </div>
                                                <div class=\"price-card price-card--highlight\">
                                                        <h2 class=\"price-card__title\">200 prises</h2>
                                                        <div class=\"price-card__ribbon\"><span class=\"price-card__ribbon-text\">50 €</span></div>
                                                        <p>200 Photos</p>
                                                        <p>Individuel & groupe</p>
                                                </div>
                                                <div class=\"price-card\">
                                                        <h2 class=\"price-card__title\">Evènements</h2>
                                                        <div class=\"price-card__ribbon\"><span class=\"price-card__ribbon-text\">100 €</span></div>
                                                        <p>Photos illimités</p>
                                                        <p>Individuel & groupe</p>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>

";
        
        $__internal_6f47bbe9983af81f1e7450e9a3e3768f->leave($__internal_6f47bbe9983af81f1e7450e9a3e3768f_prof);

        
        $__internal_5a27a8ba21ca79b61932376b2fa922d2->leave($__internal_5a27a8ba21ca79b61932376b2fa922d2_prof);

    }

    public function getTemplateName()
    {
        return "home/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 76,  167 => 72,  163 => 71,  157 => 68,  153 => 66,  149 => 65,  88 => 6,  78 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Libre &amp; vivant | Page d'accueil{% endblock %}

{% block body %}
<style>
@import url('https://fonts.googleapis.com/css2?family=Blaka&family=Calligraffitti&display=swap');
</style>

<div class=\"site-wrap\" id=\"home-section\">

        <div class=\"site-mobile-menu site-navbar-target\">
                <div class=\"site-mobile-menu-header\">
                        <div class=\"site-mobile-menu-close mt-3\">
                                <span class=\"icon-close2 js-menu-toggle\"></span>
                        </div>
                </div>
                <div class=\"site-mobile-menu-body\"></div>
        </div>
        <header class=\"site-navbar site-navbar-target\" role=\"banner\">
                <div class=\"container\">
                        <div class=\"row align-items-center position-relative\">
                                <div class=\"col-3 \">
                                        <div class=\"site-logo\">
                                                <a href=\"index.html\">L&V</a>
                                        </div>
                                </div>
                                <div class=\"col-9  text-right\">
                                        <span class=\"d-inline-block d-lg-none\">
                                                <a href=\"#\" class=\"text-white site-menu-toggle js-menu-toggle py-5 text-white\">
                                                        <span class=\"icon-menu h3 text-white\"></span>
                                                </a>
                                        </span>
                                        <nav class=\"site-navigation text-right ml-auto d-none d-lg-block\" role=\"navigation\">
                                                <ul class=\"site-menu main-menu js-clone-nav ml-auto \">
                                                        <li class=\"active\"><a href=\"/\" class=\"nav-link\">Accueil</a></li>
                                                        <li><a href=\"/photos\" class=\"nav-link\">Toutes mes photos</a></li>
                                                        <li><a href=\"/contact\" class=\"nav-link\">Contact</a></li>
                                                </ul>
                                        </nav>
                                </div>
                        </div>
                </div>
        </header>
        <div class=\"ftco-blocks-cover-1 w-100\" style=\"height: auto\">
                <div class=\"site-section-cover overlay home-banner h-100\" data-stellar-background-ratio=\"0.5\">
                        <div class=\"container h-100\">
                                <div class=\"row align-items-center justify-content-center text-center h-100\">
                                        <div class=\"col-md-12\">
                                                <h1 class=\"mb-3\">Libre & Vivant</h1>
                                                <p style=\"font-family: 'Bebas Neue', cursive;\">Bonjour, je m’appelle Victoria, je suis agée de 20 ans, j’ai fait de la photographie une passion qui permet de rendre heureux les personnes ou de laisser une émotion sur certaines photographies que personne n'arriverait à voir. Aujourd’hui je me présente a vous afin de pouvoir rendre vos moments encore plus magique et d’en faire des souvenirs inoubliable.</p>
                                                <blockquote class=\"blockquote-footer text-white\" style=\"font-family: 'Calligraffitti', cursive;\">La photographie est une brève complicité entre la prévoyance et le hasard. </blockquote>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
        <div class=\"site-section p-2 bg-black\">
                <div class=\"row\">
                        <div class=\"col-lg-12 ml-auto pl-lg-5 text-center pt-4\">
                                <h3 class=\"scissors text-center text-red\">Dernières prises!</h3>
                                <div class=\"row w-100\">
                                        <div class=\"col-12\">
                                                <div class=\"price-table pb-4\">
                                                        {% for photo in photos %}
                                                                <div class=\"product-card pt-0 p-1\">
                                                                        <div class=\"w-100\">
                                                                                <img class=\"w-100\" src=\"{{vich_uploader_asset(photo, 'imageFile')|imagine_filter('homepage')}}\"/>
                                                                        </div>
                                                                        <div class=\"d-flex justify-content-end flex-column my-2\">
                                                                                <p class=\"font-weight-bold\">{{photo.name}}</p>
                                                                                <p class=\"text-muted m-0\">Ajouté le: {{photo.postDate|date('d-m-Y')}}</p>
                                                                        </div>
                                                                </div>
                                                        {% endfor %}
                                                </div>
                                        </div>
                                </div>
                                <p class=\"p-5\"><a href=\"/photos\" class=\"btn btn-more\">En voir plus</a></p>
                        </div>
                </div>
        </div>
        <div class=\"site-section bg-black p-0 pt-2\">
                <div class=\"container\">
                        <div class=\"row justify-content-center  mb-5\">
                                <div class=\"col-md-7 text-center\">
                                        <h3 class=\"scissors text-center\">Services &amp; Prix</h3>
                                </div>
                        </div>
                        <div class=\"row\">
                                <div class=\"col-12\">
                                        <div class=\"price-table pb-4\">
                                                <div class=\"price-card\">
                                                        <h2 class=\"price-card__title\">100 prises</h2>
                                                        <div class=\"price-card__ribbon\"><span class=\"price-card__ribbon-text\">25 €</span></div>
                                                        <p>100 Photos</p>
                                                        <p>Individuel & groupe</p>
                                                </div>
                                                <div class=\"price-card price-card--highlight\">
                                                        <h2 class=\"price-card__title\">200 prises</h2>
                                                        <div class=\"price-card__ribbon\"><span class=\"price-card__ribbon-text\">50 €</span></div>
                                                        <p>200 Photos</p>
                                                        <p>Individuel & groupe</p>
                                                </div>
                                                <div class=\"price-card\">
                                                        <h2 class=\"price-card__title\">Evènements</h2>
                                                        <div class=\"price-card__ribbon\"><span class=\"price-card__ribbon-text\">100 €</span></div>
                                                        <p>Photos illimités</p>
                                                        <p>Individuel & groupe</p>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </div>
        </div>
</div>

{% endblock %}
", "home/index.html.twig", "/opt/qivalio/libre/templates/home/index.html.twig");
    }
}
