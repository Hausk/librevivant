<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/login.html.twig */
class __TwigTemplate_b72c92b6e56cbc07512f2419e6d158a47d8b292c24c616d57eb7d72c0d46825d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'javascripts' => [$this, 'block_javascripts'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/login.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        ";
        // line 9
        echo "        ";
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 12
        echo "
        ";
        // line 13
        $this->displayBlock('javascripts', $context, $blocks);
        // line 16
        echo "    </head>
    <body>
    <style>
    * {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
html {
  font-size: 62.5%;
}

body {
  font-family: \"Poppins\", sans-serif;
  line-height: 1.6;
  color: #1a1a1a;
  font-size: 1.6rem;
  overflow-x: hidden;
}
a {
  color: #2196f3;
  text-decoration: none;
}

.container {
  display: grid;
  grid-template-rows: minmax(min-content, 100vh);
  grid-template-columns: repeat(2, 50vw);
}
.heading-secondary {
  font-size: 3rem;
}
.heading-primary {
  font-size: 5rem;
}
.span-blue {
  color: #2196f3;
}
.signup-container,
.signup-form {
  display: flex;
  flex-direction: column;
  gap: 2rem;
}
.signup-container {
  width: 100vw;
  padding: 10rem 10rem;
  align-items: flex-start;
  justify-content: flex-start;

  grid-column: 1 / 2;
  grid-row: 1;
}
.signup-form {
  max-width: 45rem;
  width: 100%;
}
.text-mute {
  color: #aaa;
}

.input-text {
  font-family: inherit;
  font-size: 1.8rem;
  padding: 3rem 5rem 1rem 2rem;
  border: none;
  border-radius: 2rem;
  background: #eee;
  width: 100%;
}
.input-text:focus {
  outline-color: #2196f3;
}

.btn {
  padding: 2rem 3rem;
  border: none;
  background: #2196f3;
  color: #fff;
  border-radius: 1rem;
  cursor: pointer;
  font-family: inherit;
  font-weight: 500;
  font-size: inherit;
}
.btn-login {
  align-self: flex-end;
  width: 100%;
  margin-top: 2rem;
  box-shadow: 0 5px 5px #00000020;
}
.btn-login:active {
  box-shadow: none;
}
.btn-login:hover {
  background: #2180f9;
}
.inp {
  position: relative;
}
.label {
  pointer-events: none;

  position: absolute;
  top: 2rem;
  left: 2rem;
  color: #00000070;
  font-weight: 500;
  font-size: 1.8rem;

  transition: all 0.2s;
  transform-origin: left;
}
.input-text:not(:placeholder-shown) + .label,
.input-text:focus + .label {
  top: 0.7rem;
  transform: scale(0.75);
}
.input-text:focus + .label {
  color: #2196f3;
}

.input-icon {
  position: absolute;
  top: 2rem;
  right: 2rem;
  font-size: 2rem;
  color: #00000070;
}
.input-icon-password {
  cursor: pointer;
}

.btn-google {
  color: #222;
  background: #fff;
  border: solid 1px #eee;
  padding: 1.5rem;

  display: flex;
  justify-content: center;
  align-items: center;

  box-shadow: 0 1px 2px #00000020;
}

.btn-google img {
  width: 3rem;
  margin-right: 1rem;
}

.login-wrapper {
  max-width: 45rem;
  width: 100%;
}
.line-breaker .line {
  width: 50%;
  height: 1px;
  background: #eee;
}
.line-breaker {
  display: flex;
  justify-content: center;
  align-items: center;
  color: #ccc;

  margin: 3rem 0;
}
.line-breaker span:nth-child(2) {
  margin: 0 2rem;
}

.welcome-container {
  background: #eeeeee75;
  grid-column: 2 / 3;
  grid-row: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  padding: 10rem;
}
.lg {
  font-size: 6rem;
}
.welcome-container img {
  width: 100%;
}

@media only screen and (max-width: 700px) {
  html {
    font-size: 54.5%;
  }
}

@media only screen and (max-width: 600px) {
  .signup-container {
    padding: 5rem;
  }
}
@media only screen and (max-width: 400px) {
  html {
    font-size: 48.5%;
  }

  .input-text:not(:placeholder-shown) + .label,
  .input-text:focus + .label {
    top: 0.6rem;
    transform: scale(0.75);
  }
  .label {
    font-size: 1.9rem;
  }
}

@media only screen and (max-width: 1200px) {
  .signup-container {
    grid-column: 1 / 3;
    grid-row: 1/3;
  }
  .welcome-container {
    display: none;
  }
}
</style>

<div class=\"row\" style=\"height: 100vh; overflow:hidden\">
        <main class=\"signup-container col-7 d-flex\">
                <div class=\"w-auto m-auto\">
                        ";
        // line 245
        if ((isset($context["error"]) || array_key_exists("error", $context) ? $context["error"] : (function () { throw new RuntimeError('Variable "error" does not exist.', 245, $this->source); })())) {
            // line 246
            echo "                        <div class=\"alert alert-danger\">Informations incorrect</div>
                        ";
        }
        // line 248
        echo "                        ";
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 248, $this->source); })()), "user", [], "any", false, false, false, 248)) {
            // line 249
            echo "                        <div class=\"mb-3\">
                                Vous êtes connecté en tant que ";
            // line 250
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 250, $this->source); })()), "user", [], "any", false, false, false, 250), "firstname", [], "any", false, false, false, 250), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 250, $this->source); })()), "user", [], "any", false, false, false, 250), "lastname", [], "any", false, false, false, 250), "html", null, true);
            echo "<br/>
                                <a href=\"";
            // line 251
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("app_logout");
            echo "\">Se déconnecter ?</a>
                        </div>
                        ";
        } else {
            // line 254
            echo "                        <h1 class=\"heading-primary\">Connexion<span class=\"span-blue\">.</span></h1>
                        <p class=\"text-mute\">Saisissez vos informations d'identification pour accéder à votre compte.</p>
                        <form method=\"post\" class=\"signup-form\">
                                <label class=\"inp\">
                                        <input type=\"email\" value=\"";
            // line 258
            echo twig_escape_filter($this->env, (isset($context["last_username"]) || array_key_exists("last_username", $context) ? $context["last_username"] : (function () { throw new RuntimeError('Variable "last_username" does not exist.', 258, $this->source); })()), "html", null, true);
            echo "\" name=\"email\" id=\"inputEmail\" class=\"input-text\" placeholder=\"&nbsp;\" required autofocus>
                                        <span class=\"label\">Email</span>
                                        <span class=\"input-icon\"><i class=\"fa-solid fa-envelope\"></i></span>
                                </label>
                                <label class=\"inp\">
                                        <input type=\"password\" class=\"input-text\" name=\"password\" id=\"inputPassword password\" id=\"password\" autocomplete=\"current-password\" placeholder=\"&nbsp;\" required autofocus>
                                        <span class=\"label\">Mot de passe</span>
                                        <span class=\"input-icon input-icon-password\" data-password><i class=\"fa-solid fa-eye\"></i></span>
                                        <input type=\"hidden\" name=\"_csrf_token\" value=\"";
            // line 266
            echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("authenticate"), "html", null, true);
            echo "\">
                                </label>
                        <button type=\"submit\" class=\"btn btn-login\">Se connecter</button>
                        </form>
                        ";
        }
        // line 271
        echo "                </div>
        </main>
        <div class=\"welcome-container col-5 p-0\" style=\"transform: rotate(10deg) scale(1.5);overflow: hidden;\">
                <div class=\"w-100 d-flex\" style=\"background: url('";
        // line 274
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("img/login.jpg"), "html", null, true);
        echo "'); background-size: cover;height: 100% !important;padding: 0;margin: 0;transform: rotate(-10deg); margin-left: -10%\">
                        <h1 class=\"heading-secondary m-auto text-white\">Bienvenue sur <br/><span class=\"lg ml-5\">Libre &amp; Vivant</span></h1>
                </div>
        </div>
</div>
</body>
</html>

";
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Login page!";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 9
    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 10
        echo "            ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackLinkTags("app");
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "javascripts"));

        // line 14
        echo "            ";
        echo $this->extensions['Symfony\WebpackEncoreBundle\Twig\EntryFilesTwigExtension']->renderWebpackScriptTags("app");
        echo "
        ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  422 => 14,  412 => 13,  399 => 10,  389 => 9,  370 => 5,  351 => 274,  346 => 271,  338 => 266,  327 => 258,  321 => 254,  315 => 251,  309 => 250,  306 => 249,  303 => 248,  299 => 246,  297 => 245,  66 => 16,  64 => 13,  61 => 12,  58 => 9,  52 => 5,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>{% block title %}Login page!{% endblock %}</title>
        <link rel=\"icon\" href=\"data:image/svg+xml,<svg xmlns=%22http://www.w3.org/2000/svg%22 viewBox=%220 0 128 128%22><text y=%221.2em%22 font-size=%2296%22>⚫️</text></svg>\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        {# Run `composer require symfony/webpack-encore-bundle` to start using Symfony UX #}
        {% block stylesheets %}
            {{ encore_entry_link_tags('app') }}
        {% endblock %}

        {% block javascripts %}
            {{ encore_entry_script_tags('app') }}
        {% endblock %}
    </head>
    <body>
    <style>
    * {
  margin: 0;
  padding: 0;
  box-sizing: border-box;
}
html {
  font-size: 62.5%;
}

body {
  font-family: \"Poppins\", sans-serif;
  line-height: 1.6;
  color: #1a1a1a;
  font-size: 1.6rem;
  overflow-x: hidden;
}
a {
  color: #2196f3;
  text-decoration: none;
}

.container {
  display: grid;
  grid-template-rows: minmax(min-content, 100vh);
  grid-template-columns: repeat(2, 50vw);
}
.heading-secondary {
  font-size: 3rem;
}
.heading-primary {
  font-size: 5rem;
}
.span-blue {
  color: #2196f3;
}
.signup-container,
.signup-form {
  display: flex;
  flex-direction: column;
  gap: 2rem;
}
.signup-container {
  width: 100vw;
  padding: 10rem 10rem;
  align-items: flex-start;
  justify-content: flex-start;

  grid-column: 1 / 2;
  grid-row: 1;
}
.signup-form {
  max-width: 45rem;
  width: 100%;
}
.text-mute {
  color: #aaa;
}

.input-text {
  font-family: inherit;
  font-size: 1.8rem;
  padding: 3rem 5rem 1rem 2rem;
  border: none;
  border-radius: 2rem;
  background: #eee;
  width: 100%;
}
.input-text:focus {
  outline-color: #2196f3;
}

.btn {
  padding: 2rem 3rem;
  border: none;
  background: #2196f3;
  color: #fff;
  border-radius: 1rem;
  cursor: pointer;
  font-family: inherit;
  font-weight: 500;
  font-size: inherit;
}
.btn-login {
  align-self: flex-end;
  width: 100%;
  margin-top: 2rem;
  box-shadow: 0 5px 5px #00000020;
}
.btn-login:active {
  box-shadow: none;
}
.btn-login:hover {
  background: #2180f9;
}
.inp {
  position: relative;
}
.label {
  pointer-events: none;

  position: absolute;
  top: 2rem;
  left: 2rem;
  color: #00000070;
  font-weight: 500;
  font-size: 1.8rem;

  transition: all 0.2s;
  transform-origin: left;
}
.input-text:not(:placeholder-shown) + .label,
.input-text:focus + .label {
  top: 0.7rem;
  transform: scale(0.75);
}
.input-text:focus + .label {
  color: #2196f3;
}

.input-icon {
  position: absolute;
  top: 2rem;
  right: 2rem;
  font-size: 2rem;
  color: #00000070;
}
.input-icon-password {
  cursor: pointer;
}

.btn-google {
  color: #222;
  background: #fff;
  border: solid 1px #eee;
  padding: 1.5rem;

  display: flex;
  justify-content: center;
  align-items: center;

  box-shadow: 0 1px 2px #00000020;
}

.btn-google img {
  width: 3rem;
  margin-right: 1rem;
}

.login-wrapper {
  max-width: 45rem;
  width: 100%;
}
.line-breaker .line {
  width: 50%;
  height: 1px;
  background: #eee;
}
.line-breaker {
  display: flex;
  justify-content: center;
  align-items: center;
  color: #ccc;

  margin: 3rem 0;
}
.line-breaker span:nth-child(2) {
  margin: 0 2rem;
}

.welcome-container {
  background: #eeeeee75;
  grid-column: 2 / 3;
  grid-row: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  padding: 10rem;
}
.lg {
  font-size: 6rem;
}
.welcome-container img {
  width: 100%;
}

@media only screen and (max-width: 700px) {
  html {
    font-size: 54.5%;
  }
}

@media only screen and (max-width: 600px) {
  .signup-container {
    padding: 5rem;
  }
}
@media only screen and (max-width: 400px) {
  html {
    font-size: 48.5%;
  }

  .input-text:not(:placeholder-shown) + .label,
  .input-text:focus + .label {
    top: 0.6rem;
    transform: scale(0.75);
  }
  .label {
    font-size: 1.9rem;
  }
}

@media only screen and (max-width: 1200px) {
  .signup-container {
    grid-column: 1 / 3;
    grid-row: 1/3;
  }
  .welcome-container {
    display: none;
  }
}
</style>

<div class=\"row\" style=\"height: 100vh; overflow:hidden\">
        <main class=\"signup-container col-7 d-flex\">
                <div class=\"w-auto m-auto\">
                        {% if error %}
                        <div class=\"alert alert-danger\">Informations incorrect</div>
                        {% endif %}
                        {% if app.user %}
                        <div class=\"mb-3\">
                                Vous êtes connecté en tant que {{ app.user.firstname }} {{ app.user.lastname }}<br/>
                                <a href=\"{{ path('app_logout') }}\">Se déconnecter ?</a>
                        </div>
                        {% else %}
                        <h1 class=\"heading-primary\">Connexion<span class=\"span-blue\">.</span></h1>
                        <p class=\"text-mute\">Saisissez vos informations d'identification pour accéder à votre compte.</p>
                        <form method=\"post\" class=\"signup-form\">
                                <label class=\"inp\">
                                        <input type=\"email\" value=\"{{ last_username }}\" name=\"email\" id=\"inputEmail\" class=\"input-text\" placeholder=\"&nbsp;\" required autofocus>
                                        <span class=\"label\">Email</span>
                                        <span class=\"input-icon\"><i class=\"fa-solid fa-envelope\"></i></span>
                                </label>
                                <label class=\"inp\">
                                        <input type=\"password\" class=\"input-text\" name=\"password\" id=\"inputPassword password\" id=\"password\" autocomplete=\"current-password\" placeholder=\"&nbsp;\" required autofocus>
                                        <span class=\"label\">Mot de passe</span>
                                        <span class=\"input-icon input-icon-password\" data-password><i class=\"fa-solid fa-eye\"></i></span>
                                        <input type=\"hidden\" name=\"_csrf_token\" value=\"{{ csrf_token('authenticate') }}\">
                                </label>
                        <button type=\"submit\" class=\"btn btn-login\">Se connecter</button>
                        </form>
                        {% endif %}
                </div>
        </main>
        <div class=\"welcome-container col-5 p-0\" style=\"transform: rotate(10deg) scale(1.5);overflow: hidden;\">
                <div class=\"w-100 d-flex\" style=\"background: url('{{ asset('img/login.jpg') }}'); background-size: cover;height: 100% !important;padding: 0;margin: 0;transform: rotate(-10deg); margin-left: -10%\">
                        <h1 class=\"heading-secondary m-auto text-white\">Bienvenue sur <br/><span class=\"lg ml-5\">Libre &amp; Vivant</span></h1>
                </div>
        </div>
</div>
</body>
</html>

", "security/login.html.twig", "/opt/qivalio/libre/templates/security/login.html.twig");
    }
}
