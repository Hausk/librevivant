<?php

namespace App\Controller;

use App\Repository\CategoriesRepository;
use App\Repository\PhotosRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PhotosController extends AbstractController
{
    #[Route('/photos', name: 'app_photos')]
    public function index(PhotosRepository $photosRepository, CategoriesRepository $categoriesRepository): Response
    {
        
        return $this->render('photos/index.html.twig', [
            'data' => $photosRepository->getList(),
            'categories' => $categoriesRepository->findAll(),
        ]);
    }
}
