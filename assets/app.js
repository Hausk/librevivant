/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/css/app.css';
import "./styles/app.scss";
import './js/app.js';

import './js/jquery-3.3.1.min';
import './js/main';
import 'aos';
import 'jquery-migrate';
import 'jquery-ui';
import 'jquery.easing';
import 'jquery.sticky';
import 'bootstrap-datepicker';
import 'typed';
import 'bigpicture'
import './js/isotope-layout';
import Masonry from 'masonry-layout';
import Lightbox from 'lightbox2';
import './js/app.js';
console.log('test');

window.onload = () => {
    const grid = document.querySelector('.grided');

    const masonry = new Masonry(grid);
}
